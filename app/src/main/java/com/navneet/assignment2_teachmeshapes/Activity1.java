package com.navneet.assignment2_teachmeshapes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class Activity1 extends AppCompatActivity {

    // MARK: Debug info
    private final String TAG = "TEACH_ME_SHAPES";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "navneetkaur131995@gmail.com";
    private final String PARTICLE_PASSWORD = "Navneet113789";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "240033000447363333343435";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    TextView chooseTheAnswer, choice1, choice2, choice3;
    ImageView shapes, result;
    String imageGenerated, choiceFromParticle;
    LinearLayout layout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        chooseTheAnswer = findViewById(R.id.chooseTheAnswer);
        shapes = findViewById(R.id.shapes);
        result = findViewById(R.id.result);
        choice1 = findViewById(R.id.choice1);
        choice2 = findViewById(R.id.choice2);
        choice3 = findViewById(R.id.choice3);
        layout2 = findViewById(R.id.layout2);

        displayShapes();

        //1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

    }

    // CUSTOM FUNCTIONS

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
                // if you get the device, then go subscribe to events
                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void subscribeToParticleEvents() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "chosenAnswer",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {
                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                choiceFromParticle = event.dataPayload;
                                if (imageGenerated.equals("shape1")) {
                                    if (choiceFromParticle.contentEquals("5")) {
                                        turnParticleGreen();
                                        displayResultOnPhone();
                                    } else {
                                        turnParticleRed();
                                        displayResultOnPhone();
                                    }
                                } else if (imageGenerated.equals("shape2")) {
                                    if (choiceFromParticle.contentEquals("10")) {
                                        turnParticleGreen();
                                        displayResultOnPhone();
                                    } else {
                                        turnParticleRed();
                                        displayResultOnPhone();
                                    }
                                } else {
                                    turnParticleRed();
                                    displayResultOnPhone();
                                }
                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });


                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Success: Subscribed to events!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    public void turnParticleGreen() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("green");
                try {
                    mDevice.callFunction("answer", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Turned light green!!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void turnParticleRed() {

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("red");
                try {
                    mDevice.callFunction("answer", functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success: Turned lights red!!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    public void displayShapes() {
        int rndInt = (Math.random() < 0.5) ? 1 : 2;
        imageGenerated = "shape" + rndInt;
        int resID = getResources().getIdentifier(imageGenerated, "drawable", getPackageName());
        shapes.setImageResource(resID);
    }

    public void displayResultOnPhone(){
        if (imageGenerated.equals("shape1")) {
            if (choiceFromParticle.contentEquals("5")) {
                int resID = getResources().getIdentifier("correct", "drawable", getPackageName());
                result.setImageResource(resID);
            } else {
                int resID = getResources().getIdentifier("wrong", "drawable", getPackageName());
                result.setImageResource(resID);
            }
        } else if (imageGenerated.equals("shape2")) {
            if (choiceFromParticle.contentEquals("10")) {
                int resID = getResources().getIdentifier("correct", "drawable", getPackageName());
                result.setImageResource(resID);
            } else {
                int resID = getResources().getIdentifier("wrong", "drawable", getPackageName());
                result.setImageResource(resID);
            }
        } else {
            int resID = getResources().getIdentifier("wrong", "drawable", getPackageName());
            result.setImageResource(resID);
        }
    }
}
