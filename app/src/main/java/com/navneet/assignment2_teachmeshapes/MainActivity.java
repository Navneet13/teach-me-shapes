package com.navneet.assignment2_teachmeshapes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {

//    // MARK: Debug info
//    private final String TAG = "TEACH_ME_SHAPES";
//
//    // MARK: Particle Account Info
//    private final String PARTICLE_USERNAME = "navneetkaur131995@gmail.com";
//    private final String PARTICLE_PASSWORD = "Navneet113789";
//
//    // MARK: Particle device-specific info
//    private final String DEVICE_ID = "240033000447363333343435";
//
//    // MARK: Particle Publish / Subscribe variables
//    private long subscriptionId;
//
//    // MARK: Particle device
//    private ParticleDevice mDevice;

    //Other Variables
    TextView txtChooseGame;
    Button btnActivity1, btnActivity2, btnChooseRandom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        // 1. Initialize your connection to the Particle API
//        ParticleCloudSDK.init(this.getApplicationContext());
//
//        // 2. Setup your device variable
//        getDeviceFromCloud();

        txtChooseGame = findViewById(R.id.txtChooseGame);
        btnActivity1 = findViewById(R.id.btnActivity1);
        btnActivity2 = findViewById(R.id.btnActivity2);
        btnChooseRandom = findViewById(R.id.btnRandomActivity);


        btnActivity1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Activity1.class);
                startActivity(intent);
            }
        });

        btnActivity2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), Activity2.class);
                startActivity(intent);
            }
        });

        btnChooseRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int rndInt = (Math.random() < 0.5) ? 1 : 2;
                if(rndInt == 1){
                    Intent intent = new Intent(getBaseContext(), Activity1.class);
                    startActivity(intent);
                }
                 else
                {
                    Intent intent = new Intent(getBaseContext(), Activity2.class);
                    startActivity(intent);
                }
            }
        });
    }
//    /**
//     * Custom function to connect to the Particle Cloud and get the device
//     */
//    public void getDeviceFromCloud() {
//        // This function runs in the background
//        // It tries to connect to the Particle Cloud and get your device
//        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
//
//            @Override
//            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
//                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
//                mDevice = particleCloud.getDevice(DEVICE_ID);
//                return -1;
//            }
//
//            @Override
//            public void onSuccess(Object o) {
//
//                Log.d(TAG, "Successfully got device from Cloud");
//            }
//
//            @Override
//            public void onFailure(ParticleCloudException exception) {
//                Log.d(TAG, exception.getBestMessage());
//            }
//        });
//    }
//
//    public void turnParticleGreen() {
//
//        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
//            @Override
//            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
//                // put your logic here to talk to the particle
//                // --------------------------------------------
//                List<String> functionParameters = new ArrayList<String>();
//                functionParameters.add("green");
//                try {
//                    mDevice.callFunction("answer", functionParameters);
//
//                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
//                    e1.printStackTrace();
//                }
//
//                return -1;
//            }
//
//            @Override
//            public void onSuccess(Object o) {
//                // put your success message here
//                Log.d(TAG, "Success: Turned light green!!");
//            }
//
//            @Override
//            public void onFailure(ParticleCloudException exception) {
//                // put your error handling code here
//                Log.d(TAG, exception.getBestMessage());
//            }
//        });
//    }
//
//    public void turnParticleRed() {
//
//        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
//            @Override
//            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
//                // put your logic here to talk to the particle
//                // --------------------------------------------
//                List<String> functionParameters = new ArrayList<String>();
//                functionParameters.add("red");
//                try {
//                    mDevice.callFunction("answer", functionParameters);
//
//                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
//                    e1.printStackTrace();
//                }
//
//                return -1;
//            }
//
//            @Override
//            public void onSuccess(Object o) {
//                // put your success message here
//                Log.d(TAG, "Success: Turned lights red!!");
//            }
//
//            @Override
//            public void onFailure(ParticleCloudException exception) {
//                // put your error handling code here
//                Log.d(TAG, exception.getBestMessage());
//            }
//        });
//
//    }
//
//    public void subscribeToParticleEvents() {
//        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
//            @Override
//            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
//                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
//                        "playerChoice",  // the first argument, "eventNamePrefix", is optional
//                        new ParticleEventHandler() {
//                            public void onEvent(String eventName, ParticleEvent event) {
//                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
//                                String choice = event.dataPayload;
//                                if (choice.contentEquals("A")) {
//                                    turnParticleGreen();
//                                }
//                                else if (choice.contentEquals("B")) {
//                                    turnParticleRed();
//                                }
//
//                            }
//
//                            public void onEventError(Exception e) {
//                                Log.e(TAG, "Event error: ", e);
//                            }
//                        });
//
//
//                return -1;
//            }
//
//            @Override
//            public void onSuccess(Object o) {
//                Log.d(TAG, "Success: Subscribed to events!");
//            }
//
//            @Override
//            public void onFailure(ParticleCloudException exception) {
//                Log.d(TAG, exception.getBestMessage());
//            }
//        });
//}


}

