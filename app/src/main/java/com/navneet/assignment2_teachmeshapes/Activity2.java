package com.navneet.assignment2_teachmeshapes;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class Activity2 extends AppCompatActivity {

    // MARK: Debug info
    private final String TAG = "TEACH_ME_SHAPES";

    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "navneetkaur131995@gmail.com";
    private final String PARTICLE_PASSWORD = "Navneet113789";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "240033000447363333343435";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;

    //Other Variables
    Button checkImageButton,flashShape;
    SignaturePad signaturePad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        //1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();

        signaturePad = findViewById(R.id.signature_pad);
        checkImageButton = findViewById(R.id.checkImageButton);
        flashShape = findViewById(R.id.flashShape);

        checkImageButton.setOnClickListener(view -> {

            signaturePad.clear();
        });

        flashShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawShapes();
            }
        });

    }

    public static class Pixel {
        final public int _x;
        final public int _y;
        public Pixel(int x,int y){
            _x=x;
            _y=y;
        }
    }

    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
                // if you get the device, then go subscribe to events
//                subscribeToParticleEvents();
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void drawShapes() {
        Toast.makeText(getApplicationContext(), "On pressed", Toast.LENGTH_SHORT)
                .show();

        int rndInt = (Math.random() < 0.5) ? 1 : 2;
        String funcName = "drawshape"+rndInt;

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                // put your logic here to talk to the particle
                // --------------------------------------------

                // what functions are "public" on the particle?
                Log.d(TAG, "Available functions: " + mDevice.getFunctions());


                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add("draw");
                try {
                    mDevice.callFunction(funcName, functionParameters);

                } catch (ParticleDevice.FunctionDoesNotExistException e1) {
                    e1.printStackTrace();
                }
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                // put your success message here
                Log.d(TAG, "Success!");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                // put your error handling code here
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }
//    public void subscribeToParticleEvents() {
//        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
//            @Override
//            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
//                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
//                        "accelerated",  // the first argument, "eventNamePrefix", is optional
//                        new ParticleEventHandler() {
//                            public void onEvent(String eventName, ParticleEvent event) {
//                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
////                                choiceFromParticle = event.dataPayload;
//                                 String eventsName = eventName;
//                                 String pitch = event.dataPayload;
//                                 System.out.println(pitch);
//                            }
//
//                            public void onEventError(Exception e) {
//                                Log.e(TAG, "Event error: ", e);
//                            }
//                        });
//
//
//                return -1;
//            }
//
//            @Override
//            public void onSuccess(Object o) {
//                Log.d(TAG, "Success: Subscribed to events!");
//            }
//
//            @Override
//            public void onFailure(ParticleCloudException exception) {
//                Log.d(TAG, exception.getBestMessage());
//            }
//        });
//
//    }
}
